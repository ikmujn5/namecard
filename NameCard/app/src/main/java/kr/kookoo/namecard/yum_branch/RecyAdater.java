package kr.kookoo.namecard.yum_branch;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import kr.kookoo.namecard.R;

public class RecyAdater extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    ArrayList<RecyItem> mdata;
    Context context;

    private final int A_Type = 0 , B_Type = 1 , C_Type = 2; //타입

    public RecyAdater(Context context , ArrayList<RecyItem> mdata){
        this.context = context;
        this.mdata = mdata;
    }


    public class B_type_ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        public B_type_ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        switch (viewType){
            case A_Type:{
                View v = LayoutInflater.from(context).inflate(R.layout.a_type , parent , false);
                return new A_Type_Layout(v);
            }
            case B_Type:{
                View v = LayoutInflater.from(context).inflate(R.layout.b_type , parent , false);
                return new B_type_ViewHolder(v);
            }
            case C_Type:{
                View v = LayoutInflater.from(context).inflate(R.layout.c_type , parent , false);
                return new C_Type_Layout(v,context);
            }

        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof A_Type_Layout){
            ((A_Type_Layout)holder).textview2(mdata.get(position).getName());
            //((A_Type)holder).textView.setText(mdata.get(position).getName());
        }
        else if(holder instanceof B_type_ViewHolder){
            ((B_type_ViewHolder)holder).imageView.setVisibility(View.VISIBLE);
        }
        else if(holder instanceof C_Type_Layout){

        }

    }

    @Override
    public int getItemViewType(int position) { //뷰 타입을 나누는곳
        switch (mdata.get(position).getType()){ //RecyItem 의 type 으로 나눈다
            case 0 : return A_Type;
            case 1 : return B_Type;
            case 2 : return C_Type;
        }
        return 0;
    }


    @Override
    public int getItemCount() {
        return mdata.size();
    }



}
