package kr.kookoo.namecard.layout_test.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import kr.kookoo.namecard.R;
import kr.kookoo.namecard.layout_test.LayoutTestActivity;

public class InnerViewPagerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    LayoutTestActivity act;
    LayoutTestActivity.Data_Type_1 item;

    int item_position = -1;

    final int PAGE_TYPE_ITEM = 0;
    final int PAGE_TYPE_DELETE = 1;

    public InnerViewPagerAdapter(Context context,  LayoutTestActivity.Data_Type_1 item,Activity act,int item_position) {
        this.context = context;
        this.item = item;
        this.act = (LayoutTestActivity)act;
        this.item_position = item_position;
    }

    public void setItem(LayoutTestActivity.Data_Type_1 item) {
        this.item = item;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if(viewType == PAGE_TYPE_ITEM){
            View view = LayoutInflater.from(context).inflate(R.layout.item_layout_profile,parent,false);
            return new Item_ViewHolder(view);
        }else if(viewType == PAGE_TYPE_DELETE){
            View view = LayoutInflater.from(context).inflate(R.layout.item_layout_delete,parent,false);
            return new Delete_ViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof Item_ViewHolder){
            ((Item_ViewHolder)holder).tv_name.setText(item.getName());
            ((Item_ViewHolder)holder).tv_tel.setText(item.getTel());
        }else if(holder instanceof Delete_ViewHolder){
            ((Delete_ViewHolder)holder).tv_delete.setOnClickListener(view -> act.deleteItem(item_position));
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    class Item_ViewHolder extends RecyclerView.ViewHolder{
        TextView tv_name,tv_tel;
        public Item_ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.item_tv_name);
            tv_tel = itemView.findViewById(R.id.item_tv_tel);
        }
    }

    class Delete_ViewHolder extends RecyclerView.ViewHolder{
        TextView tv_delete;
        public Delete_ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_delete = itemView.findViewById(R.id.item_tv_delete);

        }
    }

}
