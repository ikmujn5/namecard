package kr.kookoo.namecard.yum_branch;

public class RecyItem {
    String name;
    int type;

    public RecyItem(int type) {
        this.type = type;
    }

    public RecyItem(String name , int type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }
}
