package kr.kookoo.namecard.yum_branch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Function3;
import kr.kookoo.namecard.R;

public class Main_index extends AppCompatActivity {
    RecyclerView recyclerview;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<RecyItem> mdata;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_index);
        context = this;
        recyclerview = findViewById(R.id.recyclerview);

        mdata = new ArrayList<RecyItem>();
        //mdata = getData(5);
        mdata.add(new RecyItem(1)); //타입이 1이면 이미지
        mdata.add(new RecyItem(2));
        for(int i=0; i<10; i++){
            mdata.add(new RecyItem(String.valueOf(i),0)); //타입이 0이면 글
        }
        recyclerview.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(Main_index.this);
        recyclerview.setLayoutManager(layoutManager);
        adapter = new RecyAdater(context,mdata);
        recyclerview.setAdapter(adapter);


    }

    public ArrayList<RecyItem> getData(int count){
        ArrayList<RecyItem> result = new ArrayList<>();


        Observable.range(0,count)
                .map(i->new RecyItem(i))
                .filter(item-> item.getType() < 3 )
                .subscribe(item -> { result.add(item); });

        return result;
    }

}
