package kr.kookoo.namecard.yum_branch;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import kr.kookoo.namecard.R;

public class C_Type_Layout extends RecyclerView.ViewHolder {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    ArrayList<C_Type_item> mdata;
    Context context;
    public C_Type_Layout(@NonNull View itemView , Context context) {
        super(itemView);
        this.context = context;
        recyclerView = itemView.findViewById(R.id.recycler_c);

        mdata = new ArrayList<C_Type_item>();

        for(int i=0; i<10; i++){
            mdata.add(new C_Type_item(String.valueOf(i),0)); //타입이 0이면 글
        }
        layoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL ,false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new C_Type_Adater(context,mdata);
        recyclerView.setAdapter(adapter);
    }
}
