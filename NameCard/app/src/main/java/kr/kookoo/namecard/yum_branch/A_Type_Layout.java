package kr.kookoo.namecard.yum_branch;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import kr.kookoo.namecard.R;

public class A_Type_Layout extends RecyclerView.ViewHolder {

    TextView textView;

    public A_Type_Layout(@NonNull View itemView) {
        super(itemView);
        textView = itemView.findViewById(R.id.textview);
    }

    public void textview2(String text){
        textView.setText(text);
    }
}
