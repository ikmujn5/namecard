package kr.kookoo.namecard.yum_branch;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import kr.kookoo.namecard.R;

public class A_Type extends RecyclerView.ViewHolder{

    TextView textView;
    public A_Type(@NonNull View itemView) {
        super(itemView);
        textView = itemView.findViewById(R.id.text);
    }

    public void textview2(String text){
        textView.setText(text);
    }
}
