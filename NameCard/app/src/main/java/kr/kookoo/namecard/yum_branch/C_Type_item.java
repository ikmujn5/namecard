package kr.kookoo.namecard.yum_branch;

public class C_Type_item {
    String id;
    int type;

    public C_Type_item(int type) {
        this.type = type;
    }

    public C_Type_item(String id, int type) {
        this.id = id;
        this.type = type;
    }

    public String getId() {
        return id;
    }
}
