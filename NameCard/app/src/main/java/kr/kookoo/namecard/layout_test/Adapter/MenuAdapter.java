package kr.kookoo.namecard.layout_test.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import java.util.ArrayList;

import kr.kookoo.namecard.R;
import kr.kookoo.namecard.layout_test.LayoutTestActivity;

public class MenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<LayoutTestActivity.Data_Type_1> datas;
    Context context;
    LayoutTestActivity act;

    final int VIEW_TYPE_ONE = 1;

    public MenuAdapter(Context context, ArrayList<LayoutTestActivity.Data_Type_1> datas, Activity act){
        this.act = (LayoutTestActivity) act;
        this.context = context;
        this.datas = datas;
    }

    public void setDatas(ArrayList<LayoutTestActivity.Data_Type_1> datas) {
        this.datas = datas;
    }

    @Override
    public int getItemViewType(int position) {
        return datas.get(position).getItem_type();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if(viewType == VIEW_TYPE_ONE){
             View view = LayoutInflater.from(context).inflate(R.layout.layout_viewpager2_100dp,parent,false);
             return new ViewPager_ViewHolder(view);
        }


        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof ViewPager_ViewHolder){
            ((ViewPager_ViewHolder)holder).viewPager2.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
            InnerViewPagerAdapter adapter = new InnerViewPagerAdapter(context,datas.get(position),act,position);
            ((ViewPager_ViewHolder)holder).viewPager2.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
            ((ViewPager_ViewHolder)holder).viewPager2.setAdapter(adapter);
        }

    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    class ViewPager_ViewHolder extends RecyclerView.ViewHolder{
        ViewPager2 viewPager2;
        public ViewPager_ViewHolder(@NonNull View itemView) {
            super(itemView);
            viewPager2 = itemView.findViewById(R.id.viewpager_100dp);

        }
    }
}
