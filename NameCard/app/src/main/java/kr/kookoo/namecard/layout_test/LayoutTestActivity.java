package kr.kookoo.namecard.layout_test;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

import io.reactivex.Observable;
import kr.kookoo.namecard.R;
import kr.kookoo.namecard.layout_test.Adapter.MenuAdapter;

public class LayoutTestActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    Context context = LayoutTestActivity.this;
    MenuAdapter mAdapter;
    ArrayList<Data_Type_1> datas = new ArrayList<>();

    Button btn_AddItem;
    EditText et_ItemCnt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout_test);

       datas =  getData(5);

        recyclerView = findViewById(R.id.recycler_a);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        mAdapter = new MenuAdapter(context,datas,this);
        recyclerView.setAdapter(mAdapter);

        et_ItemCnt = findViewById(R.id.et_item_cnt);
        btn_AddItem = findViewById(R.id.btn_add);

        btn_AddItem.setOnClickListener(view -> {
            int count = Integer.parseInt(et_ItemCnt.getText().toString());
            et_ItemCnt.setText("");
            datas.addAll(getData(count));
        });
    }

    public ArrayList<Data_Type_1> getData(int count){
        ArrayList<Data_Type_1> result = new ArrayList<>();


        Observable.range(0,count)
                    .map(i -> new Data_Type_1(i+"","010-"+"000"+i+"-000"+i,1))
                    .subscribe(data_type_1 -> { result.add(data_type_1);});

        return result;
    }

    public void deleteItem(int position){
        Log.e("=====","position  "+position);

       if(datas.size() != 0) {
           datas.remove(position);
           mAdapter.setDatas(datas);
           mAdapter.notifyItemRemoved(position);
           mAdapter.notifyItemRangeChanged(position,datas.size());
       }
    }

    public class Data_Type_1 {

        String name = "";
        String tel = "";
        int item_type = 0;

        public Data_Type_1(){}

        public Data_Type_1(String name, String tel, int item_type) {
            this.name = name;
            this.tel = tel;
            this.item_type = item_type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public int getItem_type() {
            return item_type;
        }

        public void setItem_type(int item_type) {
            this.item_type = item_type;
        }
    }
}
