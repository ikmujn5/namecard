package kr.kookoo.namecard;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.assertTrue;

public class MainActivity extends AppCompatActivity {

    String[] permissions = {Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE};
    boolean isPermissionsGranted = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!isPermissionsGranted){
            permissionCheck(MainActivity.this,permissions);
        }

    }



    public void permissionCheck(Context context, String[] permissions){

        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                Toast.makeText(context, "권한 허가", Toast.LENGTH_SHORT).show();
                isPermissionsGranted = true;
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(context, "권한 거부\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                isPermissionsGranted = false;
            }
        };

        TedPermission.with(context)
                .setPermissionListener(permissionlistener)
                .setRationaleMessage("서비스 이용을 위해 필요한 권한을 허용해주세요")
                .setDeniedMessage("권한이 없으면, 서비스를 정상적으로 이용할수없습니다. \n [설정] > [권한] 에서 권한을 허용해주세요.")
                .setPermissions(permissions)
                .check();
    }


/**
 * 자바 테스트 용 임시 메서드 양식
 * */
    @Test
    public void testGuGuDan_Imperative(){
        int dan = 4;
        for(int row = 1; row<=9; row++){
            System.out.println(dan+ " * "+row+" = "+(dan * row));
        }

        assertTrue(true);
    }
}
